const init = require('./index.js')

const config = {
  client: 'better-sqlite3',
  database: 'main.db',
  version: 0,
}

;(async () => {
  const {TName} = await init(config, [], {
    TName: {
      name: String,
      pass: {type: String, maxLength: 64, notNull: false},
      created: {type: Date, notNull: false},
      deleted: Date,
      isVip: Boolean,
      deep: {
        other: String,
        field: {
          mode: Boolean,
          nono: Date,
        },
      },
      $virtual: Boolean,
      arbitrary: {type: JSON, default: []},
    }
  })
  console.log(TName.toString())
  const res = await TName.update({isVip: true},[{id: 1}, {name: 'value', deep: {other: {$gte: 3}}}], {
    fields: {id: true, deep: {other: true}}, limit: 3, skip: 5,
  })

  const items = await TName.create([{name: 'João'}, {name: 'Jonas'}])
  console.log(items)

  const get = await TName.select({id: {$gte: 2}})
  console.log(require('util').inspect(get, {depth: null, colors: true}))

  //console.log(TName.create({name: 'Olá'}))
})()
