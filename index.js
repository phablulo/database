const define     = require('./main.js')
const {resolve}  = require('path')

module.exports = async function init(config, miggrations, tables) {
  let db
  if (config.client === 'better-sqlite3') {
    const Database   = require('better-sqlite3')

    const name = config.database
    if (!name.endsWith('.db')) name += '.db'
    db = new Database(name, {verbose: null})
    if (process.env.SQLITE_REGEX) {
      try { db.loadExtension(resolve(__dirname, 'pcre2.so')) }
      catch(e) {
        console.warn('\x1b[33mEnv var SQLITE_REGEX is set, but pcre2.so was not found.')
        console.warn('Regex won\'t work until you install this:')
        console.warn('https://gitlab.com/phablulo/sqlite3-pcre2')
        console.warn('Or use the get-pcre2.sh script to install automatically.\x1b[0;m')
      }
    }
  }
  else {
    throw new Error('database '+config.client+' is not yet implemented.')
  }

  const currentVersion = db.pragma('user_version')[0].user_version
  const version = config.version || 0
  if ((currentVersion || 0) < version) {
    // migrations!
    db.prepare('BEGIN EXCLUSIVE').run()
    try {
      for (const migrate of miggrations.slice(version)) {
        migrate(db)
      }
      db.pragma('user_version = '+version)
      db.prepare('COMMIT').run()
    }
    catch (e) {
      db.prepare('ROLLBACK').run()
      throw e
    }
  }

  const tableList = Object.create(null)
  const $sql = async (query, values=[]) => {
    const stmt = db.prepare(query.toString())
    if (query._method === 'select') return Promise.resolve(stmt.all(values)) // select
    return Promise.resolve(stmt.run(values))
  }
  const sql = (process.env.VERBOSE
    ? (q,v) => console.log('query:', q.toString()) || $sql(q,v)
    : $sql
  )

  db.pragma('foreign_keys = ON')
  const result = {db, sql}
  for (let [name,value] of Object.entries(tables)) {
    const table = result[name] = (Array.isArray(value)
      ? define(value[0], value[1], tableList, sql)
      : define(name, value, tableList, sql)
    )
    if (process.env.VERBOSE) console.log('query:', table.toString())
    db.exec(table.toString())
  }

  return result
}

