
# Database

A very simple and incomplete ORM.

**Warning**: This is a personal toy project. It's unstable and untested. Please do not use it.

## Install

```bash
yarn add https://gitlab.com/phablulo/database
yarn add better-sqlite3
```

## Usage

Creating table schema:
```javascript
const init        = require('database')

const config = {
  client: 'better-sqlite3',
  database: 'name.db',
  version: 0,
}
const miggrations = []
const result = await init(config, miggrations, {
  tableName: {
    field1: String|Number|Boolean|Date|JSON|'1:1'|'n:1'|'1:n',
    field2: {
      type: String,
      maxLength: 500,
      notNull: false,
      unique: false,
      default: 'olá',
      hide: false, // if true, don't automatically fetch it on select queries
    },
    field3: {
      type: Number,
      float: false,
    },
    field4: {
      type: Number,
      setter: x => x + 1,
      getter: x => x - 1,
    },
    nested: {
      field: {
        is: String,
        supported: {type: Boolean, default: true},
      }
    },
    foreign:  {
      type: '1:1'|'1:n'|'n:1',
      references: 'sql_table_name',
    },
    $ignored: Boolean, // won't be saved on db
    $alsoIgnore: String,
  },
  aliasName: ['sqlName', {
    field: String,
  }],
  table: {
    field1: Number,
    field2: Number,
    $knex(table) {
      // raw access to knex table
      table.unique(['field1','field2'])
    }
  }
})
console.log(result.tableName)
console.log(result.aliasName)
console.log(result.db) // raw access to db
console.log(result.sql) // helper method to execute sql queries
```
Nested fields use `__` as a string separator in the DB names. So `{nested: {field: ...}}` becomes `nested__field ...`.


### Table instance methods

| Name | Description
|---|---|
| `count()` | returns the number of rows in the table
| `create(objects[])` | insert objects into the table. The field `id` is automatically written
| `update(change, filter, options?)` | apply `change` to rows filtered by `filter`
| `updateOne(object)` | sincronize a single object with the table
| `select(filter, options?)` | select based on `filter`
| `selectOne(filter, options?)` | select based on `filter`, but only the first row
| `delete(filter, options?)` | delete based on `filter`
| `deleteOne(object)` | delete a single object

The `filter` field works as following:
| Filter | Result
|---|---|
| `{a: 1, c: 2}` | `where a = 1 and c = 2`
| `[{a: 1, b: 2}, {c: 2, d: 3}` | `where (a = 1 and b = 2) or (c = 2 and d = 3`
| `{a: {$[gt,gte,lt,lte,ne]: 1}}` | `where a [>,>=,<,<=,!=] 1`
| `{a: [1,2]}` | `where a in (1, 2)`

The `options` field works as following:
| Option | Result
|---|---|
| `{limit: 10}` | `limit 10`
| `{skip: 5}` | `offset 5`
| `{fields: {a: true, b: {c: 1, d: false}}` | `select a, b__c, b__d ...`
| `{order: 'field` | `order by field asc`
| `{order: ['deep.field', 'desc']}` | `order by deep__field desc`

