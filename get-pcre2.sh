#!/bin/bash

if [ -z "$( which git )" ]; then
  echo "needs to install git"
  sudo apt-get install git
fi
for lib in libsqlite3-dev libpcre2-dev; do
  if [ -z "$( locate $lib )" ]; then
    echo "needs to install $lib"
    sudo apt-get install $lib
  fi
done

git clone https://gitlab.com/phablulo/sqlite3-pcre2
cd sqlite3-pcre2
make
cd ..
mv sqlite3-pcre2/pcre2.so pcre2.so
rm -rf sqlite3-pcre2/

