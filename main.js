module.exports = define

const SEPARATOR = '__'
const knex = require('knex')({
  client: 'better-sqlite3',
  useNullAsDefault: true,
})

class Table {
  constructor(name, def) {
    this.name = name
    this.definition = def
    this.fields = Object.entries(def)
      .filter(([key,value]) => key[0] !== '$' && !value.hide)
      .map(item => name+'.'+item[0])
  }
  async count(filters) {
    if (!Array.isArray(filters)) filters = [filters]
    const query = filterQuery(filters.map(this.toFlat), {}, knex(this.name).count('id as count'))
    const rows = await this.sql(query)
    return rows[0].count
  }
  async select(filters, opt) {
    const fields = (opt?.fields
      ? Object.keys(this.toFlat$(opt.fields))
      : this.fields
    )
    if (!Array.isArray(filters)) filters = [filters]
    const init = opt?.$knex?.(knex, fields, this.name) || knex.select(fields).from(this.name)
    const query = filterQuery(filters.map(this.toFlat), opt, init)
    const rows = await this.sql(query)
    return rows.map(this.toDeep)
  }
  async selectOne(filters, opt) {
    const rows = await this.select(filters, {...opt, limit: 1})
    return rows[0] || null
  }
  find(filters, opt) { return this.select(filters, opt) }
  findOne(filters, opt) { return this.selectOne(filters, opt) }
  update(change, filters, opt) {
    if (!Array.isArray(filters)) filters = [filters]
    const query = filterQuery(filters.map(this.toFlat), opt, knex(this.name))
      .update(this.toFlat(change))
    return this.sql(query)
  }
  updateOne(id, object) {
    return this.update(object, {id}, {limit: 1})
  }
  delete(filters, opt) {
    if (!Array.isArray(filters)) filters = [filters]
    const query = filterQuery(filters.map(this.toFlat), opt, knex(this.name)).del()
    return this.sql(query)
  }
  deleteOne(object) {
    return this.delete({id: object.id}, {limit: 1})
  }
  async create(objects) {
    const isArray = Array.isArray(objects)
    if (!isArray) objects = [objects]

    const query  = knex(this.name).insert(objects.map(x => this.toFlat({...x})))
    const result = await this.sql(query)
    let id = result.lastInsertRowid - objects.length
    for (const object of objects) {
      object.id = ++id
    }
    if (!isArray) return objects[0]
    return objects
  }
  toFlat()   { throw new Error('replace me') }
  toDeep()   { throw new Error('replace me') }
  toString() { throw new Error('replace me') }
  sql()      { throw new Error('replace me') }
}

function define(name, definition, tables, execute) {
  definition = {id: {type: Number}, ...definition}
  const {$knex} = definition
  delete definition.$knex

  const table = knex.schema.createTable(name, table => {
    table.increments('id')
    const recursive = (def, prev) => {
      for (let [key,value] of Object.entries(def)) {
        if (value instanceof Function || value === JSON) {
          def[key] = value = {type: value}
        }
        const tkey = prev + key // table key

        if (key[0] === '$' || key === 'id') {
          if (!value.getter) value.getter = identity
          if (!value.setter) value.setter = identity
          continue
        }

        let field, getter, setter
        switch (value.type) {
          case String:
            field = table.string(tkey, value.maxLength || 255)
            break
          case Number:
            field = value.float ? table.float(tkey) : table.integer(tkey)
            break
          case Boolean:
            field  = table.tinyint(tkey)
            getter = i => Boolean(i)
            setter = b => (b&&1)||0
            break
          case Date:
            setter = d => {
              if (d) {
                if (d instanceof Date) return Math.floor(d.getTime()/1000)
                if (d.$in)  d.$in  = d.$in.map(d = Math.floor(d.getTime()/1000))
                if (d.$gt)  d.$gt  = Math.floor(d.$gt.getTime()/1000)
                if (d.$gte) d.$gte = Math.floor(d.$gte.getTime()/1000)
                if (d.$lt)  d.$lt  = Math.floor(d.$lt.getTime()/1000)
                if (d.$lte) d.$lte = Math.floor(d.$lte.getTime()/1000)
                if (d.$ne)  d.$ne  = Math.floor(d.$ne.getTime()/1000)
              }
              return d
            }
            getter = i => i && new Date(i * 1000)
            field  = table.integer(tkey)
            break
          case JSON:
            setter = j => j && JSON.stringify(j)
            getter = j => j && JSON.parse(j)
            field  = table.text(tkey)
            break
          case 'text':
            field = table.text(tkey)
            break
          case 'n:1':
          case '1:1': {
            // a foreign-tkey está em mim
            field = table.integer(tkey).unsigned()
            table.foreign(tkey).references(value.references+'.id')
            // setter = s => s.id ?? s
            /*
            const tb = tables[value.references]
            getter = function() {
              return tb.selectOne({id: this[tkey]})
            }
            getter.lazy = true
            */
            break
          }
          case '1:n': {
            // a foreign-key está no outro
            /*
            const tb = tables[value.references]
            const def = tb.definition
            const rf = Object.keys(def).find(x => def[x].type === 'n:1' && def[x].references === name)
            if (!rf) throw new Error('Can\'t find reference column on '+value.references)
            getter = function(q) {
              return tb.select({[rf]: this.id, ...q})
            }
            getter.lazy = true
            */
            setter = () => { throw new Error('Can\t set on 1:n!') }
            break
          }
          default:
            if (value && typeof value === 'object' && !Array.isArray(value)) {
              recursive(value, tkey + SEPARATOR)
              continue
            }
            throw new Error('Unknown type '+value)
        }

        if (value.getter) {
          if (getter) {
            const g1 = getter
            const g2 = value.getter
            getter = x => g2(g1(x))
          }
          else getter = value.getter
        }
        if (value.setter) {
          if (setter) {
            const s1 = setter
            const s2 = value.setter
            setter = x => s2(s1(x))
          }
          else setter = value.setter
        }

        if (value.notNull) field.notNullable()
        if (value.unique) field.unique()
        if (value.hasOwnProperty('default')) field.defaultTo(
          setter ? setter(value.default) : value.default
        )

        value.getter = getter || identity
        value.setter = setter || identity
      }
    }
    recursive(definition, '')
    if ($knex) $knex(table)
  })


  const sql = table.toString() // executa o builder pra colocar os getters/setters
              .replace('create table', 'create table if not exists')
              .replace(/create (\w+ )?index/, x => x+' if not exists')

  const {toFlatO,toFlatF,toDeep} = convert(definition)
  const def = toFlatF(definition)

  const $table = new Table(name, def)
  Object.defineProperties($table, {
    toDeep: {value: toDeep.bind($table)},
    toFlat: {value: toFlatO.bind($table)},
    toFlat$: {value: toFlatF.bind($table)},
    toString: {value: () => sql},
    sql: {value: execute},
  })
  return $table
}

function convert(def) {
  const recursive = (def, arr, prefix) => {
    for (const [key,value] of Object.entries(def)) {
      if (key[0] === '$') continue;
      const type = value?.type || value
      switch (type) {
        case String:
        case Number:
        case Boolean:
        case Date:
        case JSON:
        case 'text':
        case '1:n':
        case '1:1':
        case 'n:1': {
          arr.push({key: prefix + key, value})
          continue
        }
      }
      if (!type || Array.isArray(type) || typeof type !== 'object') {
        throw new Error('Invalid type for key `'+prefix+key+'`: '+type)
      }
      // tem type, não é array e é object:
      recursive(value, arr, prefix+key+SEPARATOR)
    }
  }
  let arr = []
  recursive(def, arr, '')
  const roots = [...new Set(arr.filter(x => x.key.includes(SEPARATOR)).map(x => x.key.split(SEPARATOR)[0]))]

  // to flat
  const toFlatObjectCode = (
    'const def = this.definition\n'
    + arr.map(({key, value}) => {
      if (key.includes(SEPARATOR)) {
        const dot = key.replaceAll(SEPARATOR, '?.')
        if (value.setter !== identity) {
          return 'if (source.'+dot+' !== undefined) source.'+key+' = def.'+key+'.setter(source.'+dot+')'
        }
        return 'if (source.'+dot+' !== undefined) source.'+key+' = source.'+dot
      }
      else if (value.setter !== identity) {
        return 'if (source.'+key+' !== undefined) source.'+key+' = def.'+key+'.setter(source.'+key+')'
      }
      return null
    }).filter(x => x).join('\n')+'\n'
    + roots.map(x => 'delete source.'+x).join('\n')+'\n'
    + 'return source'
  )
  const rgx = /(def\.\w+\.[sg]etter)|(const def .+\n)/g
  const toFlatO = Function('source', toFlatObjectCode)
  const toFlatF = Function('source', toFlatObjectCode.replace(rgx, ''))

  // to deep
  const struct = Object.create(null)
  for (const {key,value} of arr) {
    const parts = key.split(SEPARATOR)
    const last = parts.pop()
    const obj  = parts.reduce((p, c) =>
      p[c] || (p[c] = Object.create(null))
    , struct)
    if (parts.length || (value.getter || identity) !== identity) {
      obj[last] = '-|def.'+key+'.getter(source.'+key+')|-'
    }
    else if ((value.getter || identity) !== identity) {
      obj[last] = '-|def.'+key+'.getter(source.'+key+')|-'
    }
  }
  const toJSON = v => JSON.stringify(v).replaceAll(/("-\|)|(\|-")/g, '')
  const toDeepCode = (
    'const def = this.definition\n'
    + Object.entries(struct).map(([k,v]) => 'source.'+k+' = '+toJSON(v)).join('\n')+'\n'
    + arr.filter(x => x.key.includes(SEPARATOR)).map(x => 'delete source.'+x.key).join('\n')+'\n'
    + 'return source'
  )
  const toDeep = Function('source', toDeepCode)
  
  return {
    toFlatO, toFlatF,
    toDeep,
  }
}

function identity(x) { return x }

function filterQuery(filters, opt, init) {
  let query = init.andWhere(init => {
    filters.reduce((query, filter) => {
      return query.orWhere(builder =>
        Object.keys(filter).reduce((p, c) => {
          const v = filter[c]
          if (v && typeof v === 'object') {
            if (Array.isArray(v))    return p.whereIn(c, v)
            if (v instanceof RegExp) return p.where(c, 'REGEXP', '(?i)'+v.toString().slice(1,-2))

            for (const [key,value] of Object.entries(v)) {
              switch (key) {
                case '$in' : p = p.whereIn(c, value)    ; break;
                case '$gt' : p = p.where(c, '>',  value); break;
                case '$gte': p = p.where(c, '>=', value); break;
                case '$lt' : p = p.where(c, '<',  value); break;
                case '$lte': p = p.where(c, '<=', value); break;
                case '$ne' : {
                  if (value === null) p = p.whereNotNull(c)
                  else p = p.where(c, '!=', value)
                  break
                }
                case '$rgx': p = p.where(c, 'REGEXP' , '(?i)'+value.toString().slice(1 , -2)); break;
                default: {
                  // throw new Error('Unknown key '+key)
                  // let is slide
                }
              }
            }
            return p
          }
          return p.where(c, v)
        }, builder)
      )
    }, init)
  })
  if (opt) {
    if (opt.limit) query = query.limit(opt.limit)
    if (opt.skip) query = query.offset(opt.skip)
    if (opt.order) {
      query = query.orderBy(
        (opt.order[0] ?? opt.order).replaceAll('.', SEPARATOR),
        opt.order[1] === -1 ? 'desc' : 'asc'
      )
    }
  }
  return query
}

